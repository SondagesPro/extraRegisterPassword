��          �       �      �  B   �          ,     D  .   W  E   �  1   �     �     ~     �     �  *   �  (   �  ]     
   e     p  !   u     �     �     �     �     �  n   �     I     c     �    �  \   �  1   �  ,   )  )   V  =   �  _   �  C     �   b  '   �      	     -	  ?   F	  A   �	  �   �	  
   ]
     h
  +   n
  #   �
  
   �
     �
     �
     �
  �     )   �  (   �     
   A new %s was generated, but an error happen when send the message. A new %s was sent by email. Admin detailed response Admin notification An error happen when send the message with %s. An error happened when trying to send the email with %s you provided. Another email has been sent with %s you provided. Attention : we use the settings you use for this attribute (mandatory etc …). Only non empty attribute are used for password. Attribute used for password. Confirm Crypt password. Email template used for password reminder. Extra email used (to send for password). If user ask a password reminder, reset the password. Using token length of survey for length. Invitation None Please enter the %s you provided. Receive %s by email Register Reminder Reset password. Reset the %s Save user password crypted. This force reset password to true. Clear password (entered by admin) still usable. The %s was sent by email. The %s you provided is invalid. You must provide %s. PO-Revision-Date: 2018-01-25 08:43:23+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/2.3.1
Language: fr
Project-Id-Version: extraRegisterPassword
 Un nouveau %s a était généré, mais une erreur est survenue lors de l‘envoi du message. Un nouveau %s vous a était envoyé par courriel. Notification détaillée à l'administrateur Notification simple à l’administrateur Une erreur est survenue lors de l‘envoi du message avec %s. Un erreur est survenue lors de la tentative d'envoi du message avec le %s que vous avez fourni. Un autre message à était envoyé avec le %s que vous avez fourni. Attention : par défaut utilise les paramètres des attributs (obligatoire …). Seul les attribut non vide seront utilisé en tant que mot de passe. Attribut utilisé pour le mot de passe. Confirmation Chiffrer le mot de passe Modèle de courriel à utiliser pour le rappel de mot de passe. Modèle de courriel à utiliser (pour l‘envoi du mot de passe). Si l‘utilisateur demande un rappel du mot de passe, celui-ci sera réinitialisé. En utilisant le paramètre de longueur du code du questionnaire. Invitation Aucun Veuillez entrer le %s que vous avez fourni. Recevoir le %s par message courriel Inscrition Rappel Réinitialiser le mot de passe. Réinitialisé le %s Enregistrer le mot de passe utilisateur chiffré. Cela force la réinitialisation du mot de passe. Les mots de passe en clair (entré par l‘administrateur) restent utilisable. Le %s vous a était envoyé par courriel. Le %s que vous avez fourni est invalide. Vous devez fournir %s. 