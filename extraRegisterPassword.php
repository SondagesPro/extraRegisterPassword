<?php

/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2023 Denis Chenu <https://www.sondages.pro>
 * @copyright 2017-2023 Limesurvey Consulting <https://survey-consulting.com/>
 * @license AGPL v3
 * @version 2.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class extraRegisterPassword extends PluginBase
{
    protected $storage = 'DbStorage';
    protected static $description = 'Allow to use an token attribute like a password.';
    protected static $name = 'extraRegisterPassword';

    /** @inheritdoc */
    public $allowedPublicMethods = [];

    /** @var string|null password caption */
    private $passwordAttributeCaption;

    /**
    * Add function to be used in beforeQuestionRender event
    */
    public function init()
    {
        /* Survey setting */
        $this->subscribe('beforeSurveySettings', 'beforeSurveySettings');
        $this->subscribe('newSurveySettings', 'newSurveySettings');

        /* Plugin register action */
        $this->subscribe('beforeRegisterForm');
        $this->subscribe('beforeRegister');

        /* Start survey */
        $this->subscribe('beforeSurveyPage');
    }

    /**
     * Set the attribute as password in JS
     */
    public function beforeRegisterForm()
    {
        /* Do the input in password if exist */
        $event = $this->event;
        $surveyId = $event->get('surveyid');
        $passwordAttribute = $this->get('passwordAttribute', 'Survey', $surveyId, null);
        if ($passwordAttribute) {
            $script = "$('#register_{$passwordAttribute}').attr('type','password').attr('autocomplete','new-password');";
            App()->getClientScript()->registerScript("extraRegisterPassword", $script, CClientScript::POS_END);
        }
        /* Or replace form ? */
    }

    /**
     * Register to afterTokenSave if needed
     */
    public function beforeRegister()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        if (!Yii::app()->request->getIsPostRequest()) {
            return;
        }
        $beforeRegister = $this->getEvent();
        $surveyId = $beforeRegister->get("surveyid");
        $attributeKey = $this->get('passwordAttribute', 'Survey', $surveyId, null);
        if (empty($attributeKey)) {
            return;
        }
        if (empty(App()->getRequest()->getPost('register_' . $attributeKey))) {
            return;
        }

        if ($this->get('emailExtra', 'Survey', $surveyId, '')) {
            $this->subscribe('beforeTokenEmail', 'sendExtraEmail');
        }
        if ($this->get('passwordCrypt', 'Survey', $surveyId, 0)) {
            $this->subscribe('afterTokenSave');
            $this->subscribe('afterTokenDynamicSave');
        }
    }

    /**
     * Action to hash password
     */
    public function afterTokenSave()
    {
        $this->unsubscribe('afterTokenSave');
        $this->unsubscribe('afterTokenDynamicSave');
        $saveEvent = $this->getEvent();
        $token = $saveEvent->get('model');
        $surveyid  = $saveEvent->get('surveyId');
        if (!$this->get('passwordCrypt', 'Survey', $surveyid, 0)) {
            return;
        }
        $attributeKey = $this->get('passwordAttribute', 'Survey', $surveyid, null);
        if (empty($attributeKey)) {
            return;
        }
        if (empty($token->getAttribute($attributeKey))) {
            return;
        }
        $encryptedAttributes = $token->getAllEncryptedAttributes($surveyid, 'TokenDynamic');
        $attributeValue = $token->getAttribute($attributeKey);
        if (in_array($attributeKey, $encryptedAttributes)) {
            $attributeValue = LSActiveRecord::decryptSingle($attributeValue);
        }
        $hashed = password_hash($attributeValue, PASSWORD_DEFAULT);
        if (in_array($attributeKey, $encryptedAttributes)) {
            $hashed = LSActiveRecord::encryptSingle($hashed);
        }
        Token::model($surveyid)->updateByPk($token->tid, [$attributeKey => $hashed]);
    }
    /**
     * Send the extra email before sending token one
     */
    public function sendExtraEmail()
    {
        $emailEvent = $this->getEvent();
        $surveyId = $emailEvent->get('survey');
        $emailtemplate = $this->get('emailExtra', 'Survey', $surveyId, '');
        if (empty($emailtemplate)) {
            return;
        }
        $this->unsubscribe('beforeTokenEmail');
        /* Ok send own email with own body */
        $mailer = $emailEvent->get('mailer');
        $previousSubject = $emailEvent->get('subject');
        $previousBody = $emailEvent->get('body');
        $mailer->aAttachements = [];
        $mailer->setTypeWithRaw($emailtemplate, $mailer->mailLanguage);
        $mailer->emailType = 'extraregisterpassword_register';
        if (!$mailer->sendMessage()) {
            \Yii::log("Extra password email not sent", \CLogger::LEVEL_WARNING, 'plugin.extraRegisterPassword.sendExtraEmail');
        }
        /* reset to register email */
        $mailer->setTypeWithRaw('register', $mailer->mailLanguage);
        $mailer->emailType = 'register';
        $mailer->addAttachementsByType();
        $mailer->Subject = $previousSubject;
        $mailer->Body = $previousBody;
    }

    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        $surveyId = $event->get('survey');
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aAvailableAttributes = $this->ownGetAttributesList($surveyId);
        if (!empty($aAvailableAttributes)) {
            //$aAvailableAttribute['none'] = $this->translate("None");
            $tokenAttributeSettings = array(
                'passwordAttribute' => array(
                    'type' => 'select',
                    'htmlOptions' => array(
                        'empty' => $this->translate("None"),
                    ),
                    'label' => $this->translate("Attribute used for password."),
                    'options' => $aAvailableAttributes,
                    'current' => $this->get('passwordAttribute', 'Survey', $surveyId, null),
                    'help' => $this->translate("Attention : we use the settings you use for this attribute (mandatory etc …). Only non empty attribute are used for password."),
                ),
                'emailExtra' => array(
                    'type' => 'select',
                    'htmlOptions' => array(
                        'empty' => $this->translate("None"),
                    ),
                    'label' => $this->translate("Extra email used (to send for password)."),
                    'options' => array(
                        'invite' => $this->translate("Invitation"),
                        'remind' => $this->translate("Reminder"),
                        //'register'=>$this->translate("Register"),
                        'confirm' => $this->translate("Confirm"),
                        'admin_notification' => $this->translate("Admin notification"),
                        'admin_responses' => $this->translate("Admin detailed response"),
                    ),
                    'current' => $this->get('emailExtra', 'Survey', $surveyId, null),
                ),
                'emailPasswordReminder' => array(
                    'type' => 'select',
                    'htmlOptions' => array(
                        'empty' => $this->translate("None"),
                    ),
                    'label' => $this->translate("Email template used for password reminder."),
                    'options' => array(
                        'invite' => $this->translate("Invitation"),
                        'remind' => $this->translate("Reminder"),
                        'register' => $this->translate("Register"),
                        'confirm' => $this->translate("Confirm"),
                        'admin_notification' => $this->translate("Admin notification"),
                        'admin_responses' => $this->translate("Admin detailed response"),
                    ),
                    'current' => $this->get('emailPasswordReminder', 'Survey', $surveyId, 'invite'),
                ),
                'passwordCrypt' => array(
                    'type' => 'boolean',
                    'label' => $this->translate("Crypt password."),
                    'help' => $this->translate("Save user password crypted. This force reset password to true. Clear password (entered by admin) still usable."),
                    'current' => $this->get('passwordCrypt', 'Survey', $surveyId, 0),
                ),
                'passwordReset' => array(
                    'type' => 'boolean',
                    'label' => $this->translate("Reset password."),
                    'help' => $this->translate("If user ask a password reminder, reset the password. Using token length of survey for length."),
                    'current' => $this->get('passwordReset', 'Survey', $surveyId, 0),
                ),
            );
            $event->set("surveysettings.{$this->id}", array(
                'name' => get_class($this),
                'settings' => $tokenAttributeSettings,
            ));
        }
    }

    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $event->get('survey'), null);
        }
    }

    /**
     * Disable access with second password, show a form
     */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->getEvent()->get("surveyId");
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            return;
        }
        if (!$oSurvey->getHasTokensTable($surveyId)) {
            return;
        }
        $token = App()->getRequest()->getParam('token');
        if ($token) {
            $passwordAttribute = $this->get('passwordAttribute', 'Survey', $surveyId, null);
            if (!$passwordAttribute) {
                return;
            }
            $surveySession = App()->session['survey_' . $surveyId];
            if (isset($surveySession['token']) && $surveySession['token'] == $token) {
                return;
            }
            $oToken = Token::model($surveyId)->findByAttributes(array('token' => $token));
            if (empty($oToken) || empty($oToken->$passwordAttribute)) {
                return;
            }
            $oToken->decrypt();
            $this->actionOnSurveyPage($surveyId, $oToken);
        }
    }

    /**
     * Show the password form
     * @param integer $surveyId
     * @param string $token
     * @return void|null
     */
    private function actionOnSurveyPage($surveyId, $oToken)
    {
        $passwordAttribute = $this->get('passwordAttribute', 'Survey', $surveyId, '');
        $error = null;
        /* Maybe password post is OK ? */
        if (App()->getRequest()->getPost('check') == 'checkpassword' && !is_null(App()->getRequest()->getPost('password_attribute'))) {
            $password = trim(App()->getRequest()->getPost('password_attribute'));
            if ($password) {
                if (password_verify($password, $oToken->$passwordAttribute)) {
                    return;
                }
                if ($password == $oToken->$passwordAttribute) {
                    return;
                }
                $error = 'invalid';
            } else {
                $error = 'empty';
            }
        }
        $reminder = null;
        $emailPasswordReminder = $this->get('emailPasswordReminder', 'Survey', $surveyId, 'invite');
        if ($emailPasswordReminder) {
            if (App()->getRequest()->getPost('reset') == 'resetpassword') {
                $resetted = $this->sendPasswordByEmail($surveyId, $oToken->token);
                if($resetted) {
                    $reminder = 'success';
                } else {
                    $reminder = 'error';
                }
            }
        }
        /* OK we get there : show the form */
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aTokensAttributes = $oSurvey->getTokenAttributes();
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $languages = $this->getAllLanguagesStrings();
        $aSurveyInfo = getSurveyInfo($surveyId, Yii::app()->getLanguage());
        $aSurveyInfo['include_content'] = 'extraregisterpassword_form';
        $aSurveyInfo['surveyUrl'] = App()->getController()->createUrl(
            '/survey/index',
            array(
                'sid' => $surveyId,
                'lang' => App()->language
            )
        );
        $aSurveyInfo['alanguageChanger']['show'] = false;
        $alanguageChangerDatas = getLanguageChangerDatas(App()->language);
        if ($alanguageChangerDatas) {
            $aSurveyInfo['alanguageChanger']['show']  = true;
            $aSurveyInfo['alanguageChanger']['datas'] = $alanguageChangerDatas;
        }
        $extraRegisterPassword = array(
            'aToken' => $oToken->getAttributes(),
            'passwordString' => $this->getPasswordAttributeCaption($surveyId, App()->language),
            'passwordAttribute' => $this->get('passwordAttribute', 'Survey', $surveyId, ''),
            'emailExtra' => $this->get('emailExtra', 'Survey', $surveyId, ''),
            'emailPasswordReminder' => $this->get('emailPasswordReminder', 'Survey', $surveyId, 'invite'),
            'passwordCrypt' => $this->get('passwordCrypt', 'Survey', $surveyId, 0),
            'passwordReset' => $this->get('passwordReset', 'Survey', $surveyId, 0) || $this->get('passwordCrypt', 'Survey', $surveyId, 0),
            'error' => $error,
            'reminder' => $reminder,
            'languages' => $languages,
        );
        $aData = array(
            'aSurveyInfo' => $aSurveyInfo,
            'extraRegisterPassword' => $extraRegisterPassword,
            'surveyid' => $surveyId,
            'token' => $oToken->token,
        );
        $this->subscribe('getPluginTwigPath');
        Template::model()->getInstance('', $surveyId);
        App()->clientScript->registerScriptFile(Yii::app()->getConfig("generalscripts") . 'nojs.js', CClientScript::POS_HEAD);
        App()->twigRenderer->renderTemplateFromFile('layout_global.twig', $aData, false);
    }

    /**
     * See getPluginTwigPath evcent
     */
    public function getPluginTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * Get the attribute list for password
     * @param integer $surveyId
     * @return array
     */
    private function ownGetAttributesList($surveyId)
    {
        $oSurvey = Survey::model()->findByPk($surveyId);
        $aAvailableAttribute = array();
        $aTokensAttribute = $oSurvey->getTokenAttributes();
        foreach ($aTokensAttribute as $key => $aTokenAttribute) {
            $aAvailableAttribute[$key] = empty($aTokenAttribute['description']) ? $key : $aTokenAttribute['description'];
        }
        return $aAvailableAttribute;
    }

    /**
     * Reset the password and send it by email
     * @var integer $surveyId
     * @var string $token
     * @return boolean : email sent
     */
    private function sendPasswordByEmail($surveyId, $token)
    {
        $attributeKey = $this->get('passwordAttribute', 'Survey', $surveyId, null);
        if (!$attributeKey) {
            return;
        }
        $emailPasswordReminder = $this->get('emailPasswordReminder', 'Survey', $surveyId, 'invite');
        if (!$emailPasswordReminder) {
            return;
        }
        $oToken = Token::model($surveyId)->find("token = :token", array(":token" => $token));
        if (!$oToken) {
            // @todo : log error
            return;
        }
        $oToken->decrypt();
        $passwordReset = $this->get('passwordReset', 'Survey', $surveyId, 0);
        $passwordCrypt = $this->get('passwordCrypt', 'Survey', $surveyId, 0);
        if ($passwordReset || $passwordCrypt) {
            $iTokenLength = Survey::model()->findByPk($surveyId)->oOptions->tokenlength;
            $iTokenLength = $iTokenLength ? $iTokenLength : 15;
            $newPassword = str_replace(
                array('~','_'),
                array('a','z'),
                Yii::app()->securityManager->generateRandomString($iTokenLength)
            );
            $oToken->$attributeKey = $newPassword;
            $oToken->encryptSave();
        }
        if ($this->sendPasswordMessage($surveyId, $token, $emailPasswordReminder)) {
            if ($passwordCrypt) {
                $oToken->decrypt();
                $oToken->$attributeKey = password_hash($newPassword, PASSWORD_DEFAULT);
                $oToken->encryptSave();
            }
            return true;
        }
        return false;
    }

    /**
     * Send the message by email
     * @var integer $surveyId
     * @var string $token
     * @var string $emailtype
     * @return boolean
     */
    private function sendPasswordMessage($surveyId, $token, $emailtype)
    {
        $language = App()->language;
        $aSurveyInfo = getSurveyInfo($surveyId, $language);
        $oToken = Token::model($surveyId)->find("token = :token", array(":token" => $token))->decrypt();
        $mailer = new \LimeMailer();
        $mailer->setSurvey($surveyId);
        $mailer->setToken($oToken->token);
        $mailer->setTypeWithRaw($emailtype, $language);
        $mailer->emailType = 'extraregisterpassword_reminder';
        $mailer->replaceTokenAttributes = true;
        return $mailer->sendMessage();
    }

    /**
     * Get current attribute caption for pasword
     * @var integer $surveyId
     * @var string $language
     * @return string|null
     */
    private function getPasswordAttributeCaption($surveyId, $language = null)
    {
        if (!is_null($this->passwordAttributeCaption)) {
            return $this->passwordAttributeCaption;
        }
        if (empty($language)) {
            $language = App()->getLanguage();
        }
        $attributeKey = $this->get('passwordAttribute', 'Survey', $surveyId, null);
        if (!$attributeKey) {
            $this->passwordAttributeCaption = "";
            return;
        }
        $aSurveyInfo = getSurveyInfo($surveyId, $language);
        if (isset($aSurveyInfo['attributecaptions'][$attributeKey]) && trim($aSurveyInfo['attributecaptions'][$attributeKey])) {
            $this->passwordAttributeCaption = $aSurveyInfo['attributecaptions'][$attributeKey];
            return $this->passwordAttributeCaption;
        }
        if (isset($aSurveyInfo['attributedescriptions'][$attributeKey]['description']) && trim($aSurveyInfo['attributedescriptions'][$attributeKey]['description'])) {
            $this->passwordAttributeCaption = $aSurveyInfo['attributedescriptions'][$attributeKey]['description'];
            return $this->passwordAttributeCaption;
        }
        $this->passwordAttributeCaption = $attributeKey;
        return $this->passwordAttributeCaption;
    }

    /**
     * get needed language strings
     * @return string[]
     */
    private function getAllLanguagesStrings()
    {
        return array(
            'Continue' => $this->translate("Continue"),
            // With password as string
            "Please enter the password you provided." => $this->translate("Please enter the %s you provided."),
            "Receive password by email" => $this->translate("Receive password by email"),
            "Receive a new password by email" => $this->translate("Receive a new password by email"),
            "The password you provided is invalid." => $this->translate("The password you provided is invalid."),
            "You must provide password." => $this->translate("You must provide password."),
            "A new password was sent by email." => $this->translate("A new password was sent by email."),
            "The password was sent by email." => $this->translate("The password was sent by email."),
            "An error happen when send the message with password." => $this->translate("An error happen when send the message with password."),
            "A new password was generated, but an error happen when send the message." => $this->translate("A new password was generated, but an error happen when send the message."),
            // Replacing pasword by attribute
            "Please enter the %s you provided." => $this->translate("Please enter the %s you provided."),
            "Receive %s by email" => $this->translate("Receive %s by email"),
            "Receive a new %s by email" => $this->translate("Receive a new %s by email"),
            "The %s you provided is invalid." => $this->translate("The %s you provided is invalid."),
            "You must provide %s." => $this->translate("You must provide %s."),
            "A new %s was sent by email." => $this->translate("A new %s was sent by email."),
            "The %s was sent by email." => $this->translate("The %s was sent by email."),
            "An error happen when send the message with %s." => $this->translate("An error happen when send the message with %s."),
            "A new %s was generated, but an error happen when send the message." => $this->translate("A new %s was generated, but an error happen when send the message."),
        );
    }
    /**
     * get translation
     * @param string
     * @param string
     * @param string language
     * @see parent::gT
     * @return string
     */
    private function translate($string, $escapeMode = 'unescaped', $language = null)
    {
        return parent::gT($string, $escapeMode, $language);
    }
}
